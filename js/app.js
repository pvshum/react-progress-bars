/** @jsx React.DOM */

var App = React.createClass({

    getInitialState: function() {
        return {selected: this.props.model.bars[1]};
    },

    handleBarSelection: function(bar, event) {
        this.setState({selected: bar});
    },

    handleDelta: function (event) {
        var delta  = parseInt(event.target.value, 10);
        var bar    = this.state.selected;
        var result = parseInt(bar.value, 10) + delta;
        bar.value  = (result >= 0) ? result : bar.value;
        bar.limit  = (bar.value > this.props.model.limits.max) ? 1 : 0;
        this.setState({selected: bar});
    },

    render: function () {
        return (
            <div>
                <ProgressBars
                model              = {this.props.model}
                selected           = {this.state.selected}
                handleBarSelection = {this.handleBarSelection}
                />
                <ControlButtons
                    model              = {this.props.model}
                    handleDelta        = {this.handleDelta}
                />
                <BarSelector
                    model              = {this.props.model}
                    handleBarSelection = {this.handleBarSelection}
                    selected           = {this.state.selected}
                />
            </div>
        );
    }
});


var ProgressBars = React.createClass({

    render: function () {

        var listElements = _.map(this.props.model.bars, function (bar) {
            var style = {
                width: [bar.value, '%'].join('')
            };
            var cx = React.addons.classSet;
            var classesBar = cx({
                'bar'   : true,
                'active': this.props.selected.id == bar.id
            });
            var classesProgress = cx({
                'progress' : true,
                'limit'    : !!bar.limit
            });
            var classesValue = cx({'barValue' : true});

            return (
                React.DOM.div({
                    className : classesBar,
                    onClick   : this.props.handleBarSelection.bind(event, bar),
                    children  : [
                        React.DOM.div({
                            className : classesProgress,
                            style     : style
                            }),
                        React.DOM.div({
                            className : classesValue,
                            children  : [bar.value, '%'].join('')
                        })
                    ]
                })
            );
        }, this);
        return (
            React.DOM.div({
                children: listElements
            })
        );
    }
});

var BarSelector = React.createClass({

    shouldComponentUpdate: function (nextProps, nextState) {
        return false;
    },

    handleChange: function (event) {
        var id  = event.target.value;
        var bar = _.filter(this.props.model.bars, function(bar) { return bar.id == id})[0];
        this.props.handleBarSelection(bar);
    },

    render: function ()  {
        var listElements = _.map(this.props.model.bars, function (bar, i) {
            return (
                <option value={bar.id}>
                    {bar.name}
                </option>
            );
        }, this);
        return (
            <select
                ref      = "barSelector"
                onChange = {this.handleChange}
                value    = {this.props.selected.id}>
                {listElements}
            </select>
        );
    }
});

var ControlButtons = React.createClass({

    render: function () {
        var listElements = _.map(this.props.model.btns, function (btn) {
            return (
                <button
                    type      = "button"
                    className = "btn btn-primary"
                    onClick   = {this.props.handleDelta}
                    value     = {btn.value}>
                    {btn.value}
                </button>
                );
        }, this);
        return (
            <div className="controls-btns">
                {listElements}
            </div>
        );
    }
});


React.renderComponent(
    App({
        model: {
            limits: {
                max: 100,
                min: 0
            },
            bars: [
                {id: 1, value: '25', name: '#progress1', limit: 0},
                {id: 2, value: '50', name: '#progress2', limit: 0},
                {id: 3, value: '75', name: '#progress3', limit: 0}
            ],
            btns: [
                {id: 1, value: '-25'},
                {id: 2, value: '-10'},
                {id: 3, value: '+10'},
                {id: 4, value: '+25'},
            ]
        }
    }),
    document.getElementById('example')
);